(ns multiof3and5.core
  (:gen-class))


(defn multiof3&5
  "Returns 0 unless input is multiple of 3 or 5"
  [n]
  (cond
    (or (= 0 (mod n 3))(= 0 (mod n 5))) n
    :else 0))


(defn -main
  "print sum of multiples of 3 and 5 under 100"
  [& args]
  (println (reduce + (map multiof3&5 (range 1000)))))
