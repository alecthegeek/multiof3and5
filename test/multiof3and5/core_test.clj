(ns multiof3and5.core-test
  (:use clojure.test
        multiof3and5.core))

(deftest a-test
  (testing "multiof3&5"
    (is (= 0 (multiof3&5 0)))
    (is (= 0 (multiof3&5 1)))
    (is (= 0 (multiof3&5 2)))
    (is (= 3 (multiof3&5 3)))
    (is (= 0 (multiof3&5 4)))
    (is (= 5 (multiof3&5 5)))
    (is (= 6 (multiof3&5 6)))
    (is (= 0 (multiof3&5 7)))
    (is (= 9 (multiof3&5 9)))
    (is (= 10 (multiof3&5 10)))
    (is (= 15 (multiof3&5 15)))
        ))
